module.exports = {
    productionSourceMap: false,
    publicPath: '/wp-content/themes/vuefrontend/components/dist/',
    outputDir: './dist',
    configureWebpack: {
        entry: './src/main.js',
        output: {
            filename: 'js/build.js'
        },
    },
    chainWebpack: config => {
        config.optimization.delete('splitChunks')
    }
};