import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    page: '',
    posts: '',
    photos: '',
    theID: '',
    post: '',
    pageLoading: true,
    postsLoading: true,
    postLoading: true,
    photosLoading: true
  },
  mutations: {
    SET_PAGE: (state, page) => {
      state.page = page.page
      state.pageLoading = false
    },
    SET_PAGEID: (state, pageId) => {
      state.theID = pageId
    },
    SET_POSTS: (state, posts) => {
      state.posts = posts
      state.postsLoading = false
    },
    SET_PHOTOS: (state, photos) => {
      state.photos = photos.posts
      state.photosLoading = false
    },
    SET_POST: (state, post) => {
      state.post = post.post
      state.postLoading = false
    }
  },
  actions: {
    LOAD_PAGE_ID: ({ commit }, idData) => {
      commit('SET_PAGEID', idData)
    },
    LOAD_PAGE: ({ commit, state }) => {
      state.pageLoading = true
      var id = state.theID
      axios.post(window.location.origin + '/graphql', {
        query: `query GET_PAGE{
          pageBy(pageId: ` + id + `){
            id
            pageId
            title
            date
            uri
            content
            defaults {
              subtitle
              featured{
                sourceUrl
              }
              sectionPack {
                sectionName
                sectionFields {
                  content
                  fieldGroupName
                  subtitle
                  title
                  image {
                    mediaItemUrl
                  }
                }
              }
            }
          }
        }`
      })
      .then((response) => {
        commit('SET_PAGE', { page: response.data.data.pageBy })
      })
    },
    LOAD_POSTS: ({ commit, state }) => {
      state.postsLoading = true
      axios.post(window.location.origin + '/graphql', {
        query: `query GET_PAGE {
          posts {
            edges {
              node {
                postId
                title
                uri
                slug
                link
                id
                featuredImage {
                  sourceUrl
                }
              }
            }
          }
        }
        `
      })
      .then((response) => {
        commit('SET_POSTS', { posts: response.data.data.posts.edges })
      })
    },
    LOAD_PHOTOS: ({ commit, state }) => {
      state.photosLoading = true
      axios.post(window.location.origin + '/graphql', {
        query: `query TheArts {
          arts (first: 250, after: null) {
            edges {
              node {
                artId
                featuredImage {
                  sourceUrl
                }
              }
            }
          }
        }
        `
      })
      .then((response) => {
        commit('SET_PHOTOS', { posts: response.data.data.arts.edges })
      })
    },
    LOAD_POST: ({ commit, state }) => {
      state.postLoading = true
      var id = state.theID
      axios.post(window.location.origin + '/graphql', {
        query: `query getaPost {
          postBy(slug: "` + id + `") {
            title
            content
            defaults {
              subtitle
              featured {
                sourceUrl
              }
            }
          }
        }
        `
      })
      .then((response) => {
        commit('SET_POST', { post: response.data.data.postBy })
      })
    },

  },
  modules: {
  }
})
