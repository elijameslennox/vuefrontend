import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueAnime from '../node_modules/vue-animejs'

Vue.use(VueAnime)


Vue.config.productionTip = false

Vue.config.devtools = true

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

/* router.beforeRouteUpdate ((to, from, next) => {
  this.$store.dispatch('LOAD_PAGE_ID', to.params.pageID)
  this.$store.dispatch('LOAD_PAGE')
  next()
}) */