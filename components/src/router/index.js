import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Blog from '../views/Blog.vue'
import Posts from '../views/Posts.vue'
import Post from '../views/Post.vue'
import Galleries from '../views/Galleries.vue'
import Rona from '../views/Rona.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: '/',
    component: Home,
    props: {
      pageID: 2
    }
  },
  {
    path: '/rona',
    name: 'rona',
    component: Rona
  },
  {
    path: '/gallery',
    name: 'gallery',
    component: Galleries,
    props: {
      pageID: 152
    }
  },
  {
    path: '/blog',
    name: 'blog',
    component: Blog,
    children: [
      { path: '/:pageID', component: Post, props: true, name: 'post' },
      { path: '', component: Posts, props: { pageID: 53 } }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: '/',
  routes
})


export default router
