<?php

function load_styles() {

    // STYLES
    wp_enqueue_style('eli', get_template_directory_uri() . '/style.css', array(), filemtime(get_stylesheet_directory() . '/style.css'));

}
add_action( 'wp_enqueue_scripts', 'load_styles' );

function load_vue_scripts() {
    wp_register_script('components', get_stylesheet_directory_uri() . '/components/dist/js/build.js', array(), false, true);
    wp_enqueue_script('components');

}
add_action('wp_enqueue_scripts', 'load_vue_scripts');

?>