<?php

add_action( 'init', 'create_art' );
function create_art() {
    register_post_type( 'art',
        array(
            'labels' => array(
                'name' => 'Art',
                'singular_name' => 'Art',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New',
                'edit' => 'Edit',
                'edit_item' => 'Edit',
                'new_item' => 'New',
                'view' => 'View',
                'view_item' => 'View',
                'search_items' => 'Search',
                'not_found' => 'None found',
                'not_found_in_trash' => 'None found in Trash',
                'parent' => 'Parent'
            ),

            'public' => true,
            'menu_position' => 7,
            'supports' => array( 'title', 'thumbnail', 'excerpt' ),
            'taxonomies' => array( '' ),
	        'show_in_rest' => true,
            'has_archive' => true,
            'show_in_graphql' => true,
            'hierarchical' => true,
            'graphql_single_name' => 'Art',
            'graphql_plural_name' => 'Arts',
        )
    );
}

add_action( 'init', 'create_sample' );
function create_sample() {
    register_post_type( 'sample',
        array(
            'labels' => array(
                'name' => 'Sample',
                'singular_name' => 'Sample',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New',
                'edit' => 'Edit',
                'edit_item' => 'Edit',
                'new_item' => 'New',
                'view' => 'View',
                'view_item' => 'View',
                'search_items' => 'Search',
                'not_found' => 'None found',
                'not_found_in_trash' => 'None found in Trash',
                'parent' => 'Parent'
            ),

            'public' => true,
            'menu_position' => 8,
            'supports' => array( 'title', 'thumbnail', 'excerpt' ),
            'taxonomies' => array( '' ),
	        'show_in_rest' => true,
            'has_archive' => true
        )
    );
}

add_action( 'init', 'create_question_pack' );
function create_question_pack() {
    register_post_type( 'q-pack',
        array(
            'labels' => array(
                'name' => 'Question Pack',
                'singular_name' => 'Question Pack',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New',
                'edit' => 'Edit',
                'edit_item' => 'Edit',
                'new_item' => 'New',
                'view' => 'View',
                'view_item' => 'View',
                'search_items' => 'Search',
                'not_found' => 'None found',
                'not_found_in_trash' => 'None found in Trash',
                'parent' => 'Parent'
            ),

            'public' => true,
            'menu_position' => 5,
            'supports' => array( 'title', 'thumbnail', 'excerpt' ),
            'taxonomies' => array( '' ),
	        'show_in_rest' => true,
            'has_archive' => true,
            'show_in_graphql' => true,
            'hierarchical' => true,
            'graphql_single_name' => 'QuestionPack',
            'graphql_plural_name' => 'QuestionPacks',
        )
    );
}
?>