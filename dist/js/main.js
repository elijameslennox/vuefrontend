/*! Eli: a VueJS theme v0.0.1 | (c) 2020 Eli Lennox | MIT License | https://www.gitlab.com/elijameslennox */
document.addEventListener('click', (function (event) {
	if (!event.target.matches('#click-me')) return;
	alert('You clicked me!');
}), false);